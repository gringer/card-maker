# Equipment Card Maker

library(shiny);
library(grid);
library(gridtext);
library(tidyverse);
library(readxl);

## zip(zipfile, files)
## tempdir()


makeCard <- function(MIMR.num, item.desc,
                     angel1.name, angel1.ext,
                     angel2.name=NULL, angel2.ext=NULL,
                     cardSize="large", booking=TRUE, doBorder=TRUE,
                     cardScale = 1){
    gtf <- gpar(fontsize=(if(cardSize == "large") {12} else {8}) * cardScale, 
                col="black");
    gtl <- gpar(fontsize=(if(cardSize == "large") {15} else {12}) * cardScale, 
                fontface=2, col="black");
    gtb <- gpar(fontsize=(if(cardSize == "large") {15} else {12}) * cardScale, 
                fontface=2, col="darkred");
    gts <- gpar(fontsize=(if(cardSize == "large") {8} else {6}) * cardScale, 
                col="black");
    Sys.setlocale(category = "LC_ALL", locale = "C");
    current.date <- format(Sys.Date(), "%Y-%b-%d");
    if(cardSize == "large"){
        pushViewport(viewport(width=unit(90 * cardScale, units = "mm"),
                              height=unit(53 * cardScale, units="mm")));
    } else {
        pushViewport(viewport(width=unit(65 * cardScale, units = "mm"),
                              height=unit(35 * cardScale, units="mm")));
    }
    if(doBorder){
        grid.rect(width=1, height=1, just = "centre",
                  gp=gpar(lwd=2));
    }
    grid.text("Equipment Angels", x=0.05, y=0.93,
              gp=gtl, just = "left");
    grid.text("Name:", x=0.05, y=0.80,
              gp=gtf, just="left");
    grid.text(angel1.name, x=0.45, y=0.675,
              gp=gtf, just="right");
    grid.text("Ext #:", x=0.05, y=0.55,
              gp=gtf, just="left");
    grid.text(angel1.ext, x=0.45, y=0.55,
              gp=gtf, just="right");
    grid.text("Name:", x=0.05, y=0.35,
              gp=gtf, just="left");
    grid.text(angel2.name, x=0.45, y=0.225,
              gp=gtf, just="right");
    grid.text("Ext #:", x=0.05, y=0.10,
              gp=gtf, just="left");
    grid.text(angel2.ext, x=0.45, y=0.10,
              gp=gtf, just="right");
    grid.text(paste0("[current as at ", current.date, "]"),
              x=0.95, y=0.12, gp=gts, just="right");
    grid.lines(x=0.5, y=c(0.8,0.1), gp=gpar(lwd=2));
    grid.text(paste0("MIMR", sub("^MIMR", "", MIMR.num)), x=0.75, y=0.75,
              gp=gtl, just="centre");
    grid.text(item.desc, x=0.75, y=0.6,
              gp=gtf, just="centre");
    if(booking){
        grid.text("Requires\nbooking", x=0.75, y=0.35,
                  gp=gtb, just="centre");
    }
    upViewport();
}

makeCardSheet <- function(cardData,
                          cardSize="large", cardScale=1, doBorder=TRUE, 
                          displayOnly=TRUE, ...){
    nRows = if(cardSize=="large") {5} else {8};
    nCols = if(cardSize=="large") {2} else {3};
    cdRows <- nrow(cardData);
    if(displayOnly){ ## only show first page
        cdRows <- min(nRows * nCols, cdRows);
    }
    for(pp in 0:(ceiling(cdRows / (nRows * nCols))-1)){
        grid.newpage();
        pushViewport(viewport(width=unit(210*cardScale, "mm"),
                              height=unit(297*cardScale, "mm")));
        if(displayOnly){
            grid.rect(width=1, height=1, just = "centre",
                      gp=gpar(lwd=2, col="lightgrey"));
        }
        if(cardSize == "large"){
            pushViewport(viewport(width=unit(180*cardScale, "mm"),
                                  height=unit(265*cardScale, "mm"),
                                  layout=grid.layout(nrow=nRows, ncol=nCols),
                                  x=unit(0, "npc") + unit(15*cardScale, "mm"), 
                                  y=unit(1, "npc") - unit(18.5*cardScale, "mm"),
                                  just=c("left", "top")));
        } else {
            pushViewport(viewport(width=unit(195*cardScale, "mm"),
                                  height=unit(280*cardScale, "mm"),
                                  layout=grid.layout(nrow=nRows, ncol=nCols),
                                  x=unit(0.5, "npc"), y=unit(0.5, "npc")));
        }
        for(xp in 0:(nCols-1)){
            for(yp in 0:(nRows-1)){
                cardRow <- pp * (nCols * nRows) + yp * nCols + xp;
                angel1.name <- cardData$Angel1[cardRow %% cdRows + 1];
                angel2.name <- cardData$Angel2[cardRow %% cdRows + 1];
                angel1.ext <- cardData$Ext1[cardRow %% cdRows + 1];
                angel2.ext <- cardData$Ext2[cardRow %% cdRows + 1];
                MIMR.num <- cardData$MIMR[cardRow %% cdRows + 1];
                item.desc <- cardData$Description[cardRow %% cdRows + 1];
                pushViewport(viewport(layout.pos.col=xp+1, layout.pos.row=yp+1));
                #grid.text(sprintf("(%d, %d)", xp, yp), 
                #          0, 1, just=c("left", "top"));
                makeCard(cardSize=cardSize, cardScale=cardScale, doBorder=doBorder,
                         angel1.name=angel1.name, angel1.ext=angel1.ext,
                         angel2.name=angel2.name, angel2.ext=angel2.ext,
                         MIMR.num=MIMR.num, item.desc=item.desc,
                         ...);
                upViewport();
            }
        }
    }
}

# Define server logic required to draw a histogram
shinyServer(function(input, output) {
    values <- reactiveValues();
    values$cardData <- read_xlsx("Example_data.xlsx");

    output$cardPlot <- renderPlot({
        filePath <- "";
        if(!is.null(input$sigFile)){
            filePath <- input$sigFile$datapath;
        }
        makeCardSheet(cardData=values$cardData,
            cardSize=input$cardSize, booking=input$requiresBooking,
            doBorder=input$addBorder, cardScale=input$cardScale);
    })
    
    output$downloadExample <- downloadHandler(
        filename = "Example_data.xlsx",
        contentType =
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        content = function(outFileName) {
            file.copy(from="Example_data.xlsx", to=outFileName);
        }
    );
    
    output$makeCards <- downloadHandler(
        filename = function() {
            if(!is.null(input$cardInputFile) && (nrow(input$cardInputFile) > 0)){
                "cards.zip";
            } else {
                "card_example.pdf";
            }
        },
        content = function(outFileName) {
            cairo_pdf(outFileName, width=8.3, height=11.7, onefile=TRUE);
            makeCardSheet(cardData=values$cardData,
                          cardSize=input$cardSize, booking=input$requiresBooking,
                          doBorder=input$addBorder, cardScale=1, displayOnly=FALSE);
            invisible(dev.off());
        },
        contentType = if(!is.null(input$cardInputFile) && (nrow(input$cardInputFile) > 0)){
            "application/zip";
        } else {
            "application/pdf";
        }
        
    )
    
    ## File upload observer
    observeEvent(input$cardInputFile, {
        input.tbl <- read_xlsx(input$cardInputFile$datapath);
        if(!all(c("MIMR", "Description", "Angel1", "Ext1", 
                  "Angel2", "Ext2") %in% colnames(input.tbl))){
            showModal(modalDialog("Error: card data file must have the following field headings:",
                                  tags$ul(tags$li("MIMR"), 
                                          tags$li("Description"), 
                                          tags$li("Angel1"), 
                                          tags$li("Ext1"), 
                                          tags$li("Angel2"), 
                                          tags$li("Ext2"))));
        } else {
            values$cardData <- input.tbl;
        }
    });

})
